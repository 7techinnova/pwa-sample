import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { lastValueFrom } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class NotificationService {
  constructor(private http: HttpClient) {}

  addSubscription(sub: PushSubscription) {
    return lastValueFrom(
      this.http.post('https://pwa-sample-server.onrender.com/api/subscription', { sub })
    );
  }

  notifications(data: string) {
    console.log('notifications : ', data);
    return lastValueFrom(
      this.http.post('https://pwa-sample-server.onrender.com/api/notifications', { data })
    );
  }
}
