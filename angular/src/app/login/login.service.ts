import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { lastValueFrom } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  constructor(private http: HttpClient) {}

  login({ username, password }: { username: string; password: string }) {
    console.log(username,password)
    return lastValueFrom(
      this.http.post('https://pwa-sample-server.onrender.com/api/login', {
        username,
        password,
      })
    );
  }
}
